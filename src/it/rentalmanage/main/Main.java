package it.rentalmanage.main;

import it.rentalmanage.it.rentalmanage.test.Test;
import it.rentalmanage.model.*;
import it.rentalmanage.model.filemanager.IFileManager;
import it.rentalmanage.model.filemanager.JSonFileManager;
import it.rentalmanage.view.MainFrame;
import org.json.JSONArray;

import java.util.*;

/**
 * Created by Jacopo Galosi on 22/02/2016.
 */
public class Main {

    public static void main(String [] args){
        IFileManager  fileManager = new JSonFileManager();

        if(!fileManager.searchFileFromName("PersonList").exists()){
            fileManager.createEmptyFile("PersonList");
        }

        if (!fileManager.searchFileFromName("VehicleList").exists()){
            fileManager.createEmptyFile("VehicleList");
        }

        if (!fileManager.searchFileFromName("PersonHistoryList").exists()){
            fileManager.createEmptyFile("PersonHistoryList");
        }

        if (!fileManager.searchFileFromName("VehicleHistoryList").exists()){
            fileManager.createEmptyFile("VehicleHistoryList");
        }

        String filePersonData = fileManager.readFile(fileManager.searchFileFromName("PersonList"));
        JSONArray jArray = fileManager.writeToJArray(filePersonData);
        List<IPerson> personList = fileManager.fromJArrayToIPersonList(jArray);

        String fileCarData = fileManager.readFile(fileManager.searchFileFromName("VehicleList"));
        JSONArray jcarArr = fileManager.writeToJArray(fileCarData);
        List<IVehicle> carList = fileManager.fromJArrayToIVehicleList(jcarArr);

        String filePersonHistory = fileManager.readFile(fileManager.searchFileFromName("PersonHistoryList"));
        JSONArray jPersHistArr = fileManager.writeToJArray(filePersonHistory);
        List<IPerson> personHistory = fileManager.fromJArrayToIPersonList(jPersHistArr);

        String fileCarHistory = fileManager.readFile(fileManager.searchFileFromName("VehicleHistoryList"));
        JSONArray jCarHistArr = fileManager.writeToJArray(fileCarHistory);
        List<IVehicle> carHistory = fileManager.fromJArrayToIVehicleList(jCarHistArr);

        //inserisco 2 utenti e 2 veicoli al primo avvio del programma
        Test t = new Test();
        t.twoUserTwoVehiclesTest(personList,carList);

        //stress test, usare solo una volta e poi ricommentare
        //t.stressTest(personList,carList);

        IModel model= new Model(personList,carList,personHistory,carHistory);
        model.getAllPersons();


        new MainFrame(model);
    }
}
