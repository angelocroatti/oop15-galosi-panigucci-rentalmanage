package it.rentalmanage.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Implementazione dell'interfaccia IVehicle
 * @author Jacopo Galosi
 * @see it.rentalmanage.model.IVehicle
 */
public class Vehicle implements IVehicle {

    private String manufactorer;
    private String model;
    private boolean rentable;
    private String numberPlate;
    private int rentPrice;
    private int insuranceCost;
    private Date insuranceExpiring;
    private int vehicleSeats;
    private DrivingLicense requestedLicences;
    private int vehicleTaxCost;
    private Date vehicleTaxDate;
    private int motTestCost;
    private Date motTestDate;
    private final Collection<IRentalPeriod> rentalList;

    /**
     * Costruttore di Vehicle
     * @param rentalList Collection di periodi di noleggio
     * @param motTestDate Data della revisione della macchina
     * @param motTestCost Costo della revisione
     * @param vehicleTaxDate Data di scadenza del bollo
     * @param vehicleTaxCost Costo del bollo
     * @param rentPrice Prezzo di noleggio
     * @param requestedLicences Patente richiesta per guidare il veicolo
     * @param vehicleSeats posti a sedere del veicolo
     * @param insuranceExpiring Data di scadenza dell'assicurazione
     * @param insuranceCost Costo dell'assicurazione
     * @param numberPlate targa del veicolo
     * @param manufactorer produttore del veicolo
     * @param model modello del veicolo
     * @param rentable flag che mi indica se il veicolo è noleggiabile o meno
     */
    public Vehicle(Collection<IRentalPeriod> rentalList, Date motTestDate, int motTestCost, Date vehicleTaxDate, int vehicleTaxCost,
                   int rentPrice, DrivingLicense requestedLicences, int vehicleSeats, Date insuranceExpiring, int insuranceCost, String numberPlate,
                   String manufactorer, String model, boolean rentable) {
        if( vehicleSeats == 0 || insuranceCost == 0 || motTestCost == 0 || rentPrice == 0){
            throw  new IllegalArgumentException();
        }
        if(rentalList == null || motTestDate == null || vehicleTaxDate == null || requestedLicences == null || insuranceExpiring ==null
                || numberPlate == null || manufactorer == null){
            throw new NullPointerException();
        }

        this.rentalList = new ArrayList<>(rentalList);
        this.motTestDate = motTestDate;
        this.motTestCost = motTestCost;
        this.vehicleTaxDate = vehicleTaxDate;
        this.vehicleTaxCost = vehicleTaxCost;
        this.requestedLicences = requestedLicences;
        this.vehicleSeats = vehicleSeats;
        this.insuranceExpiring = insuranceExpiring;
        this.insuranceCost = insuranceCost;
        this.numberPlate = numberPlate;
        this.manufactorer = manufactorer;
        this.rentPrice=rentPrice;
        this.model = model;
        this.rentable = rentable;
    }

    @Override
    public void setManufactorer(String manufactorer) {
         this.manufactorer = manufactorer;
    }

    @Override
    public String getManufactorer() {
        return this.manufactorer;
    }

    @Override
    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String getModel() {
        return this.model;
    }

    @Override
    public boolean isRentable() {
        return this.rentable;
    }

    @Override
    public void setRentability(boolean rentable) {
        this.rentable = rentable;
    }

    @Override
    public void setRentPrice(int rentPrice) {
        this.rentPrice= rentPrice;
    }

    @Override
    public int getRentPrice() {
        return this.rentPrice;
    }

    @Override
    public String getNumberPlate() {
        return this.numberPlate = numberPlate;
    }

    @Override
    public int getInsuranceCost() {
        return this.insuranceCost;
    }

    @Override
    public void setInsuranceCost(int insuranceCost) {
        this.insuranceCost = insuranceCost;
    }

    @Override
    public Date getInsuranceExpiring() {
        return this.insuranceExpiring;
    }

    @Override
    public void setInsuranceExpiring(Date insuranceExpiring) {
        this.insuranceExpiring = insuranceExpiring;
    }

    public int getVehicleSeats() {
        return this.vehicleSeats;
    }

    @Override
    public DrivingLicense getRequestedLicense() {
        return this.requestedLicences;
    }

    @Override
    public void setRequestedLicence(DrivingLicense drivingLicense) {
        this.requestedLicences = drivingLicense;
    }

    public int getVehicleTaxCost() {
        return this.vehicleTaxCost;
    }

    public void setVehicleTaxCost(int vehicleTaxCost) {
        this.vehicleTaxCost = vehicleTaxCost;
    }

    public Date getVehicleTaxDate() {
        return this.vehicleTaxDate;
    }

    public void setVehicleTaxDate(Date vehicleTaxDate) {
        this.vehicleTaxDate = vehicleTaxDate;
    }

    @Override
    public int getMotTestCost() {
        return this.motTestCost;
    }

    @Override
    public void setMotTestCost(int motTestCost) {
        this.motTestCost = motTestCost;
    }

    @Override
    public Date getMotTestDate() {
        return this.motTestDate;
    }

    @Override
    public void setMotTestDate(Date motTestDate) {
        this.motTestDate = motTestDate;
    }

    @Override
    public Collection<IRentalPeriod> getAllTenant() {
        return new ArrayList<>(this.rentalList);
    }

    @Override
    public void addTenant(IRentalPeriod personPeriod) {
        if (personPeriod == null) {
            throw new NullPointerException();
        }
        rentalList.add(personPeriod);
    }

    @Override
    public String toString(){
        return new String("Manufactorer " + this.manufactorer+ " model " + this.model);
    }
}
