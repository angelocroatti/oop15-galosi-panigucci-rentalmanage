package it.rentalmanage.view;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Definisce i metodi che permettono alle sottoclassi di impostare delle tabelle e l'evento del bottone
 * @author nicolapanigucci
 */
public interface IStorage {

    /**
     * Imposta la tabella da visualizzare
     * @param component nuovo oggetto da visualizzare
     */
    void setTable(JComponent component);

    /**
     * Setta il listener al btnAdd
     * @param listener evento del bottone
     */
    void setAddListener(ActionListener listener);
}
