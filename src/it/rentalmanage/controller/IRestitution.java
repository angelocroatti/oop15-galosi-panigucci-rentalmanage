package it.rentalmanage.controller;

/**
 * Implementa il metodo che gestisce la restituzione di un veicolo
 * @author nicolapanigucci
 */
public interface IRestitution {

    /**
     * Gestisce la restituzione di un veicolo
     * @param choice scelta dell'utente
     */
    void restitution(final int choice);

    /**
     * Controlla se un veicolo è in noleggio
     * @return true se il veicolo può essere noleggiato
     */
    boolean isDeliveredVehicle();

}
