package it.rentalmanage.controller;

import it.rentalmanage.model.IModel;
import it.rentalmanage.model.IPerson;
import it.rentalmanage.model.filemanager.IFileManager;

/**
 * Implementazione dell'interfaccia IAddPersonController
 * @author Jacopo Galosi
 * @see it.rentalmanage.controller.IAddPersonController
 */
public class AddPersonController implements IAddPersonController {

    private IFileManager manager;
    private IModel model;

    /**
     * Costruttore di IAddPersonController
     * @param manager oggetto di tipo file manager
     * @param model oggetto di tipo model
     */
    public AddPersonController(final IFileManager manager, final IModel model) {
        this.manager = manager;
        this.model = model;
    }

    @Override
    public void writePerson(IPerson person) {
        model.addPerson(person);
        manager.writeList(manager.writePersonToJArray(model.getAllPersons().values()), "Person");

    }

}
