package it.rentalmanage.controller;

import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;
import it.rentalmanage.model.filemanager.IFileManager;

/**
 * Implementazione dell'interfaccia IAddCarController
 * @author Jacopo Galosi
 * @see it.rentalmanage.controller.IAddVehicleController
 */
public class AddVehicleController implements IAddVehicleController {

    private IFileManager manager ;
    private IModel model;

    /**
     * Costruttore di AddCarController
     * @param manager oggetto di tipo file manager
     * @param model oggetto di tipo model
     */
    public AddVehicleController(final IFileManager manager, final IModel model) {
        this.manager = manager;
        this.model = model;
    }

    @Override
    public void writeVehicle(IVehicle vehicle) {

        model.addCar(vehicle);
        manager.writeList(manager.writeVehicleToJArray(model.getAllCar().values()), "Vehicle");
    }
}
