package it.rentalmanage.controller;

import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;
import it.rentalmanage.view.ICustomTableModelVehicle;

import java.util.Map;

/**
 * Visualizza tutti i veicoli noleggiabili dai clienti
 * @author nicolapanigucci
 * @see it.rentalmanage.controller.ITableRantableController
 */
public class TableRentableController implements ITableRantableController {

    private IModel model;
    private ICustomTableModelVehicle customTableModelVehicle;

    /**
     * Costruttore di TableRentableController
     * @param model oggetto di tipo IModel
     * @param viewModel oggetto di tipo ICustomTableModelVehicle
     */
    public TableRentableController(final IModel model, final ICustomTableModelVehicle viewModel){
        this.model = model;
        this.customTableModelVehicle = viewModel;
    }

    @Override
    public void showRentableVehicle() {
        Map<String,IVehicle> vehicleMap = model.getAllCar();
        customTableModelVehicle.setElement(vehicleMap);
    }
}
