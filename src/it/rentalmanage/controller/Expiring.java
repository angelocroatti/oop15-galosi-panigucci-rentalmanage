package it.rentalmanage.controller;

import it.rentalmanage.model.IVehicle;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Controlla la validità delle date inserite e se un veicolo ha scadenze imminenti (entro 15 giorni)
 * @author nicolapanigucci
 * @see it.rentalmanage.controller.IExpiring
 */
public class Expiring implements IExpiring{

    private static final String INSURANCE = "Insurance";
    private static final String CARTAX = "Vehicle Tax";
    private static final String MOTTEST = "MOT Test";

    private IVehicle vehicle;
    private Calendar date;
    private SimpleDateFormat sdf;

    /**
     * Costruttore di Expiring
     * @param vehicle oggetto di tipo IVehicle
     */
    public Expiring(final IVehicle vehicle){
        this.vehicle = vehicle;

        if (vehicle == null){
            this.date = Calendar.getInstance();
            this.date.setTime(new Date());
        } else {
            this.date = Calendar.getInstance();
            this.date.setTime(new Date());
            this.date.add(Calendar.DAY_OF_MONTH, 15);
        }

        this.sdf = new SimpleDateFormat("dd/MM/yyyy");
    }

    @Override
    public List<String> checkExpiring(){
        List<String> exp = new ArrayList<>();

        //scadenza entro 15 giorni
        if (!vehicle.getInsuranceExpiring().after(date.getTime())){
            exp.add(INSURANCE);
        }
        if (!vehicle.getVehicleTaxDate().after(date.getTime())){
            exp.add(CARTAX);
        }
        if (!vehicle.getMotTestDate().after(date.getTime())){
            exp.add(MOTTEST);
        }

        return exp;
    }

    @Override
    public boolean checkExpiringDateInput(String date) {

        if (date.length() > 10){

            return false;
        }

        try {
            Date d = sdf.parse(date);

            Calendar cal = Calendar.getInstance();
            cal.setTime(d);

            if (cal.getTime().after(this.date.getTime())){
                return true;
            }

            return false;

        } catch (ParseException e) {

            return false;
        }
    }

    @Override
    public boolean checkUpdateExpiringDateInput(Date oldDate, String date) {

        try {
            Date newDate = sdf.parse(date);
            if (!sdf.format(oldDate).equals(sdf.format(newDate))){
                return checkExpiringDateInput(date);
            }

            return true;

        } catch (ParseException e) {
            return false;
        }

    }

    @Override
    public String getTextInsurance(){
        return INSURANCE;
    }

    @Override
    public String getTextCarTax(){
        return CARTAX;
    }

    @Override
    public String getTextMOTTest(){
        return MOTTEST;
    }
}
